package com.lucas.apibuilders.services;

import com.lucas.apibuilders.exceptions.NotFoundException;
import com.lucas.apibuilders.models.Client;
import com.lucas.apibuilders.repositories.ClientRepository;
import java.beans.FeatureDescriptor;
import java.util.Objects;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ClientService {

  private final ClientRepository repository;

  public ClientService(ClientRepository repository) {
    this.repository = repository;
  }

  public Page<Client> getList(String name, String cpf, int limit, int offset) {
    log.info("get clients");

    final Client clientFilter = Client
        .builder()
        .name(name)
        .cpf(cpf)
        .build();

    final Pageable pagination = createPageRequest(offset, limit);

    return repository.findAll(Example.of(clientFilter), pagination);
  }

  private Pageable createPageRequest(int offset, int limit) {
    return PageRequest.of(offset, limit);
  }

  public Client getOne(Long id) {
    log.info("get client");
    return repository.findById(id)
        .orElseThrow(() -> new NotFoundException("Client not found"));
  }

  public Client create(Client client) {
    log.info("create client {}", client);
    repository.save(client);

    return client;
  }

  public void update(Long id, Client client) {
    log.info("update client {}", client);

    Client clientDB = getOne(id);

    client.setId(clientDB.getId());

    try {
      BeanUtils.copyProperties(client, clientDB, getNullPropertyNames(client));
    } catch(Exception e) {
      log.error("Error copying properties of client entity");
    }
    repository.save(clientDB);
  }

  public void delete(Long id) {
    log.info("delete client {}", id);

    if(Objects.nonNull(getOne(id))) {
      repository.deleteById(id);
    }
  }

  public static String[] getNullPropertyNames(Object source) {
    final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
    return Stream.of(wrappedSource.getPropertyDescriptors())
        .map(FeatureDescriptor::getName)
        .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
        .toArray(String[]::new);
  }

}
