package com.lucas.apibuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiBuildersApplication.class, args);
	}

}
