package com.lucas.apibuilders.exceptions;

import java.util.Collection;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ControllerAdvice extends ResponseEntityExceptionHandler {

  @ExceptionHandler(ApiException.class)
  public ResponseEntity<Collection<APIErrorMessage>> handleApiException(ApiException apiException) {
    log.error("Handling", apiException);

    Collection<APIErrorMessage> message = List.of(APIErrorMessage.builder()
        .code(apiException.getHttpStatus().toString())
        .description(apiException.getKey())
        .build());

    return ResponseEntity.status(apiException.getHttpStatus()).body(message);
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public Collection<APIErrorMessage> handleException(Exception exception) {
    log.error("Handling", exception);

    return List.of(APIErrorMessage.builder()
        .code(HttpStatus.INTERNAL_SERVER_ERROR.name())
        .description(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
        .build());
  }

}
