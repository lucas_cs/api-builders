package com.lucas.apibuilders.exceptions;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class ApiException extends RuntimeException {

  private final String key;

  private final HttpStatus httpStatus;

  public ApiException(@NonNull String key) {
    super(key);
    this.key = key;
    this.httpStatus = HttpStatus.BAD_REQUEST;
  }

  public ApiException(@NonNull String key, HttpStatus httpStatus) {
    super(key);
    this.key = key;
    this.httpStatus = httpStatus;
  }

}
