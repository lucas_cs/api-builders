package com.lucas.apibuilders.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ApiException {

  public NotFoundException(String key) {
    super(key, HttpStatus.NOT_FOUND);
  }

}
