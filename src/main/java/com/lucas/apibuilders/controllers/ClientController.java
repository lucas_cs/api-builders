package com.lucas.apibuilders.controllers;

import com.lucas.apibuilders.dtos.ClientDTO;
import com.lucas.apibuilders.dtos.ClientPatchDTO;
import com.lucas.apibuilders.services.ClientService;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.Min;
import javax.validation.constraints.PositiveOrZero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@RestController
@RequestMapping("/clientes")
public class ClientController {

  private final ClientService service;

  public ClientController(ClientService service) {
    this.service = service;
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public List<ClientDTO> getList(
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "cpf", required = false) String cpf,
      @RequestParam(value = "_limit", required = false) @Min(1) int limit,
      @RequestParam(value = "_offset", required = false) @PositiveOrZero int offset
  ) {
    var clients = service.getList(name, cpf, limit, offset);
    return clients.stream()
        .map(client -> new ClientDTO().toDTO(client))
        .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ClientDTO getOne(@PathVariable Long id) {
    var client = service.getOne(id);

    return ClientDTO.builder()
        .id(client.getId())
        .name(client.getName())
        .cpf(client.getCpf())
        .dateOfBirth(client.getDateOfBirth())
        .build();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<Object> createClient(@Validated @RequestBody ClientDTO clientDTO, UriComponentsBuilder uriComponentsBuilder) {
    var client = service.create(clientDTO.toModel());

    return ResponseEntity
        .created(locationHeader(uriComponentsBuilder, client.getId()))
        .build();
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void updateClient(@PathVariable Long id, @Validated @RequestBody ClientDTO clientDTO) {
    service.update(id, clientDTO.toModel());
  }

  @PatchMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void patchClient(@PathVariable Long id, @RequestBody ClientPatchDTO clientDTO) {
    service.update(id, clientDTO.toModel());
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteClient(@PathVariable Long id) {
    service.delete(id);
  }

  private URI locationHeader(UriComponentsBuilder uriComponentsBuilder, Long id) {
    return uriComponentsBuilder.path("/clientes/{clientId}").buildAndExpand(id).toUri();
  }

}
