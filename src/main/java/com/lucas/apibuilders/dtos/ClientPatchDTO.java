package com.lucas.apibuilders.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lucas.apibuilders.models.Client;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientPatchDTO {

  private Long id;

  private String name;

  private String cpf;

  @JsonFormat(pattern="dd-MM-yyyy")
  private LocalDate dateOfBirth;

  public Client toModel() {
    return Client.builder()
        .id(this.id)
        .name(this.name)
        .cpf(this.cpf)
        .dateOfBirth(this.dateOfBirth)
        .build();
  }
}
