package com.lucas.apibuilders.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lucas.apibuilders.models.Client;
import java.time.LocalDate;
import java.time.Period;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientDTO {

  private Long id;

  @NotBlank(message = "nome é obrigatório")
  private String name;

  @NotBlank(message = "nome é obrigatório")
  private String cpf;

  @JsonFormat(pattern="dd-MM-yyyy")
  private LocalDate dateOfBirth;

  private int age;

  public int age(LocalDate dateOfBirth) {
    return Period.between(dateOfBirth, LocalDate.now()).getYears();
  }

  public Client toModel() {
    return Client.builder()
        .id(this.id)
        .name(this.name)
        .cpf(this.cpf)
        .dateOfBirth(this.dateOfBirth)
        .build();
  }

  public ClientDTO toDTO(Client client) {
    return ClientDTO.builder()
        .id(client.getId())
        .name(client.getName())
        .cpf(client.getCpf())
        .dateOfBirth(client.getDateOfBirth())
        .age(age(client.getDateOfBirth()))
        .build();
  }
}
