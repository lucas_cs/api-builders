# Builder
# FROM oracle/graalvm-ce:20.1.0-java11 AS builder
# COPY  . /root/app/
# WORKDIR /root/app
# RUN ./mvnw clean package -DskipTests

# # Application
# FROM oracle/graalvm-ce:20.1.0-java11 AS application
# COPY --from=builder /root/app/target/*.jar /home/app/
# WORKDIR /home/app
# RUN chmod 0777 /home/app
# EXPOSE 8080
# ENTRYPOINT java -jar $JAVA_OPTIONS *.jar $APP_ARGS



FROM openjdk:11-jdk-slim
COPY target/*.jar /app/app.jar
EXPOSE 8080
CMD java $JAVA_OPTS -jar /app/app.jar