## Requirements
- [docker] and [docker-compose]

[docker]: https://www.docker.com/
[docker-compose]: https://docs.docker.com/compose/

# Start

1 - Start all services db-mysql, api-builders

 ``docker-compose up --build``


Exposes endpoints:

    - GET http://localhost:8080/clientes
    - POST http://localhost:8080/clientes
    - PUT http://localhost:8080/clientes/{id}
    - DELETE http://localhost:8080/clientes/{id}
    - PATCH http://localhost:8080/clientes


Collection Postman:

    src/main/resources/postman
